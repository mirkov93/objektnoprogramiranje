#ifndef MATHERR_H
#define MATHERR_H
#include<iostream>

using namespace std;
class MathErr
{
public :
virtual void ShowError () = 0;
virtual string GetError()=0;
};
class ZeroDivide : public MathErr {
public :
void ShowError () { cout << " Dijeljenje s nulom "<<endl; }
string GetError () {return " Dijeljenje s nulom ";}
};
class Unsuported : public MathErr {
public :
void ShowError() {cout << "Nepodrzan operator "<<endl;}
string GetError () {return "Nepodrzan operator";}
};
class Nema_operatora : public MathErr {
public :
    void ShowError(){cout<<"Operator nije unesen"<<endl;}
    string GetError(){return "Operator nije unesen";}
};
class Nema_broja : public MathErr {
public:
    void ShowError(){cout<<"Broj nije unesen"<<endl;}
    string GetError(){return "Broj nije unesen";}
};

#endif // MATHERR_H
