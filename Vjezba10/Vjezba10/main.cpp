#include <iostream>
#include <fstream>
#include <string>
#include "MathErr.h"

using namespace std;
int unos_operanda ()
    {
        int a;
        cout << "Unesite cjelobrojni operand"<<endl;
        cin>>a;
        if(cin.fail())
        {
            cin.clear();
            cin.ignore();
            throw (Nema_broja());
        }
        else
        {
            return a;
        }

    }
bool provjera_operatora (char a)
    {
        if (a=='+'||a=='-'||a=='*'||a=='/')
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    char unos_operatora ()
    {
        char a;
        cout<<"Unesite operator"<<endl;
        cin >> a;
        if (cin.fail())
        {
            cin.clear();
            cin.ignore();
            throw (Nema_operatora());
        }
        else
        {
            if (provjera_operatora(a)==true)
            {
                return a;
            }
            else
            {
                throw (Unsuported());
            }
        }
    }
    int izracunaj (int a,int b,char c)
    {
        if (c=='+')
        {
            return a+b;
        }
        if (c=='-')
        {
            return a-b;
        }
        if (c=='*')
        {
            return a*b;
        }
        else
        {
            if (b==0)
            {throw (ZeroDivide());}
            else
            {
                float rez;
                rez = (float)a/b;
                return rez;
            }
        }
    }


int main()
{


    while (true){
        try{
        int x=unos_operanda();
        int y=unos_operanda();
        char operacija=unos_operatora();
        cout << izracunaj(x,y,operacija)<<endl;
        }
        catch(MathErr& a)
        {
            ofstream myfile;
            myfile.open("error.log",ios::out | ios::app);
            myfile<<a.GetError()<<endl;
            myfile.close();
            a.ShowError();
        }
        catch(...){
        cout << "Nepoznata pogreska"<<endl;
        }
    }





}
