#ifndef ARRAY_H
#define ARRAY_H
#include <stdio.h>
#include <iostream>
using namespace std;


class Array
{
    public:
        Array();
        Array (int velicina);
        virtual ~Array();
        int getvelicina ();
        friend ostream& operator << ( ostream& os , Array& a);
        friend istream& operator >>( istream&, Array& a) ;
        Array& operator = (Array& other);
        friend Array operator + (Array& a, Array& b);

    private:
        int* niz;
        int velicina;
        static int counter;
};

#endif // ARRAY_H
