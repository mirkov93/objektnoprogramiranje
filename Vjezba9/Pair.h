#ifndef PAIR_H
#define PAIR_H
#include <iostream>

using namespace std;

template<typename T1, typename T2>
class Pair
{
    T1 first;
    T2 second;
public:
    //pair() : first(T1()), second(T2()){}
    //pair(const T1& t1, const T2& t2) : first(t1), second(t2){}
    Pair(const T1& t1 = T1(), const T2& t2 = T2()) : first(t1), second(t2){}
    Pair(const Pair<T1, T2>& other)  : first(other.first), second(other.second){}
    bool operator== (const Pair<T1, T2>& other) const
    {
        return first == other.first && second == other.second;
    }
    bool operator!= (const Pair<T1, T2>& other) const
    {
        return first != other.first && second != other.second;
    }
    bool operator>= (const Pair<T1, T2>& other) const
    {
        return first >= other.first && second >= other.second;
    }
    bool operator<= (const Pair<T1, T2>& other) const
    {
        return first <= other.first && second <= other.second;
    }
    bool operator> (const Pair<T1, T2>& other) const
    {
        return first > other.first && second > other.second;
    }
    bool operator< (const Pair<T1, T2>& other) const
    {
        return first < other.first && second < other.second;
    }

    Pair<T1, T2>&operator = (const Pair<T1, T2>& other)
    {
        first = other.first;
        second = other.second;
        return *this;
    }
    friend istream & operator >> (istream &is,Pair<T1, T2>&p)
    {
        cout << "prvi broj:";
        is >> p.first;
        cout << "drugi broj:";
        is >> p.second;
    }


    friend ostream & operator << (ostream &os, const Pair<T1, T2>& a)
    {
        os << "(" << a.first << ", " << a.second << ")";
        return os;
    }

    void swap ()
    {
        Pair <T1,T2> temp = *this ;
        this->first = temp.second;
        this->second=temp.first;
    }
};
template <>
class Pair<char*, char*>{
    char* a;
    char* b;
public:
    Pair (char *a1= new char, char *b2= new char):a(a1),b(b2){}

    bool operator > (const Pair <char*, char*> & other){ return a > other.a && b > other.b;}
    bool operator < (const Pair <char*, char*> & other){ return a < other.a && b < other.b;}
    friend istream& operator >> (istream &is, Pair<char*, char*> &p) {
		is >> p.a >> p.b;
		return is;
	}
	friend ostream& operator << (ostream &os, const Pair<char*, char*> &p) {
		os << "(" << p.a << ", " << p.b << ") ";
		return os;
	}




};




#endif // PAIR_H
