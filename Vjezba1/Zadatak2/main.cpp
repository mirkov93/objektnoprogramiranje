#include <iostream>
#include <string>

using namespace std;

struct Student{
    int ID;
    string ime;
    char spol[1];
    int kviz1;
    int kviz2;
    int mid;
    int fin;
    int score;
    };
void print_student (Student& student){
    cout <<"ID:" << student.ID << "\nIme:" << student.ime << "\nSpol:" << student.spol << "\nKviz1:" << student.kviz1 << "\nKviz2:" << student.kviz2 << "\nOcjena pola semestra:"
    << student.mid << "\nOcjena na kraju semestra:" << student.fin << "\nUkupno bodova:" << student.score << endl;
}
void menu (){
        cout << "IZBORNIK\n";
        cout << "\n1.Dodaj novi zapis" << "\n2.Ukloni zapis" << "\n3.Azuriraj zapis" << "\n4.Prikazi sve zapise" << "\n5.Izracunaj prosjek bodova za studenta" <<
		 "\n6.Prikazi studenta s najvecim brojem bodova" << "\n7.Prikazi studenta s najmanjim brojem bodova" << "\n8.Pronadji studenta po ID-u" <<
		 "\n9.Sortiraj zapise po broju bodova(total)" << "\n10.Izlaz\n"<<endl;}
int main()
{
    Student razred[20];
    int len=0;
    int odabir;
    menu();
    while (true){
        cout << "Odaberite" << endl;
        cin >> odabir;
        if (odabir==1)
        {
            int f=1;
            Student s;
            cout << "Unesite studentov ID" << endl;
            cin >> s.ID;
            for (int i=0; i<=len; i++){
                if (s.ID==razred[i].ID){
                    cout << "Unijeli ste postojeci ID"<<endl;
                    f=0;
                    break;
                }
            }

            if (f>0){
                    cout << "\nIme: ";
                    cin >> s.ime;

                    cout << "\nSpol: ";
                    cin >> s.spol;

                    cout << "\nOcjena 1. kviza: ";
                    cin >> s.kviz1;

                    cout << "\nOcjena 2. kviza: ";
                    cin >> s.kviz2;

                    cout << "\nOcjena na sredini semestra: ";
                    cin >> s.mid;

                    cout << "\nOcjena na kraju semestra: ";
                    cin >> s.fin;

                    s.score = s.kviz1 + s.kviz2 + s.mid+ s.fin;
                    cout << "\nUkupan broj bodova: " << s.score << endl;
                    razred[len]=s;
                    len++;}
        }
        if (odabir==2)
        {
            int erase_id;
            cout << "Unesite ID studenta kojeg zelite obrisati" << endl;
            cin >> erase_id;
            for (int i=0; i<len; i++){
                if (razred[i].ID==erase_id){
                    razred[i]=razred[len-1];
                    len --;
                }
            }
        }
        if (odabir==3)
        {
            int update_id;
            cout << "Unesite ID studenta kojeg zelite azurirati" << endl;
            cin >> update_id;
            for (int i=0; i<len; i++){
                if (razred[i].ID==update_id)
                {
                    cout << "\nIme: ";
                    cin >> razred[i].ime;

                    cout << "\nSpol: ";
                    cin >> razred[i].spol;

                    cout << "\nOcjena 1. kviza: ";
                    cin >> razred[i].kviz1;

                    cout << "\nOcjena 2. kviza: ";
                    cin >> razred[i].kviz2;

                    cout << "\nOcjena na sredini semestra: ";
                    cin >> razred[i].mid;

                    cout << "\nOcjena na kraju semestra: ";
                    cin >> razred[i].fin;

                    razred[i].score = razred[i].kviz1 + razred[i].kviz2 + razred[i].mid+ razred[i].fin;
                    cout << "\nUkupan broj bodova: " << razred[i].score << endl;
                }
            }

        }
        if (odabir==4)
        {
            for (int k=0; k<len; k++){
                print_student(razred[k]);
            }
        }
        if (odabir==5)
        {
            int ave_id;
            cout << "Unesite ID studenta ciji prosjek trazite" << endl;
            cin >> ave_id;
            for (int i=0; i<len; i++){
                if (razred[i].ID==ave_id){
                    float avr=(float)razred[i].score/(float)4;
                    cout << "Prosjek bodova trazenog studenta je: " << avr << endl;
                }
            }
        }
        if (odabir==6)
        {
			int max = 0;
			Student maxs;
            for (int i = 0; i < len; i++)
			{
				if (razred[i].score > max)
				{
					max = razred[i].score;
					maxs = razred[i];
				}

			}

			cout << "\nStudent s najvecim brojem bodova je: " << maxs.ime << " s brojem bodova " << maxs.score << endl;

        }
        if (odabir==7)
        {
            int min = 40;
			Student mins;
            for (int i = 0; i < len; i++)
			{
				if (razred[i].score < min)
				{
					min = razred[i].score;
					mins = razred[i];
				}

			}

			cout << "\nStudent s najmanjim brojem bodova je: " << mins.ime << " s brojem bodova " << mins.score << endl;

        }
        if (odabir==8)
        {
            int find_id;
            cout << "Unesite ID trazenog studenta" << endl;
            cin >> find_id;
            for (int i=0; i<len; i++){
                if (razred[i].ID==find_id){
                    print_student(razred[i]);
                }
            }
        }
        if (odabir==9)
        {
            for (int i=0; i<len-1; i++){
                for (int j=i+1; j<len; j++){
                    if (razred[j].score>razred[i].score)
                    {
                        Student temp= razred[i];
                        razred[i]=razred[j];
                        razred[j]=temp;
                    }
                }
            }
            for (int k=0; k<len; k++){
                print_student(razred[k]);
            }

        }
        if (odabir > 9 || odabir < 1)
        {
            break;
        }

    }


    return 0;
}
