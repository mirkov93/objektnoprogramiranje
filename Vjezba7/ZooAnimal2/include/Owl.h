#ifndef OWL_H
#define OWL_H
#include "Bird.h"

class Owl : public Bird {
protected:
	int dnevni_obrok;
public:
	Owl(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje,int dnevni_obrok);

};


#endif // OWL_H
