#ifndef GRIFFONVULTURE_H
#define GRIFFONVULTURE_H
#include "Bird.h"

class GriffonVulture : public Bird {
protected:
	int dnevni_obrok;
public:
	GriffonVulture(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje,int dnevni_obrok);

};

#endif // GRIFFONVULTURE_H
