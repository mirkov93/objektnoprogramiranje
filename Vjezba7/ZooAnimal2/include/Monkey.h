#ifndef MONKEY_H
#define MONKEY_H
#include "Mammal.h"

class Monkey : public Mammal {
protected:
	int dnevni_obrok;
public:
	Monkey(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje,int dnevni_obrok);

};

#endif // MONKEY_H
