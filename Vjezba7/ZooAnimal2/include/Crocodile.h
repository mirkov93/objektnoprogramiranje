#ifndef CROCODILE_H
#define CROCODILE_H
#include "Reptile.h"

class Crocodile : public Reptile {
protected:
	int dnevni_obrok;
public:
	Crocodile(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje,int dnevni_obrok);

};


#endif // CROCODILE_H
