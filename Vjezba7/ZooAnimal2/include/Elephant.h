#ifndef ELEPHANT_H
#define ELEPHANT_H
#include "Mammal.h"

class Elephant : public Mammal {
protected:
	int dnevni_obrok;
public:
	Elephant(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje,int dnevni_obrok);

};

#endif // ELEPHANT_H
