#ifndef REPTILE_H
#define REPTILE_H
#include <iostream>
#include <string>
#include "ZooAnimal.h"

class Reptile : public ZooAnimal
{
    protected:
        int gestperiod;
        double avgtemp;
        string razmnozavanje;

    public:
        Reptile(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated, int gestperiod,double avgtemp,string razmnozavanje);
        friend std::istream& operator>>(std::istream& is, Reptile& r);
        friend std::ostream& operator<<(std::ostream& os, Reptile& r);
};
#endif // REPTILE_H
