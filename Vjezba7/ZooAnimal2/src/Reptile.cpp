#include "Reptile.h"
using namespace std;

Reptile::Reptile(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje) : ZooAnimal(vrsta, ime, god_rod, kavez, br_obroka, estimated) {
	this->gestperiod=gestperiod;
	this->avgtemp=avgtemp;
	this->razmnozavanje="UGLAVNOM POLAGANJE JAJA";
}

istream& operator>>(istream& is, Reptile& r) {
	cout << "Unesite gestacijski period: ";
	is >> r.gestperiod;
	cout << "Unesite prosje�nu temperaturu: ";
	is >> r.avgtemp;
	return is;
}

ostream& operator<<(ostream& os, Reptile& r) {
	os << "Vrsta: " << r.vrsta << "\nIme: " << r.ime << "\nGodina rodenja: " << r.god_rod <<
		"\nBroj Kaveza: " << r.kavez<< "\nBroj dnevnih obroka: " << r.br_obroka <<
		"\nOcekivani zivotni vijek: " << r.estimated << endl;
	os << "Gestacijski period: " << r.gestperiod<< "\nProsjecna temperatura: " << r.avgtemp << "\nNacin razmnozavanja: " << r.razmnozavanje << endl;
	return os;
}
