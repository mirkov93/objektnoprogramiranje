#ifndef POINT_H
#define POINT_H
#include<iostream>
#include<ctime>
#include <math.h>


class Point
{
    public:

        void setpoint(double x,double y,double z);
        void setpointrandom(double x,double y,double z);
        double getx();
        double gety();
        double getz();
        double distance2d(Point& other);
        double distance3d(Point& other);
        void printpoint();


    private:
        double x;
        double y;
        double z;
};

#endif // POINT_H
