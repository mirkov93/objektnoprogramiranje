#include "Point.h"
#include <math.h>
#include <cstdlib>
#include <iostream>
using namespace std;

void Point::setpoint(double x, double y,double z){
    this -> x = x ;
    y = y ;
    z = z ;

}


void Point::setpointrandom(double x,double y,double z){
    this -> x = rand()%(-10)+10;
    this -> y = rand()%(-10)+10;
    this -> z = rand()%(-10)+10;
    }

double Point::getx() {
    return this -> x;
}
double Point::gety() {
    return this -> y;
}
double Point::getz() {
    return this -> z;
}

double Point::distance2d(Point& other){
    return sqrt(pow(this->x-other.getx(),2)+pow(this->y-other.gety(),2));
}

double Point::distance3d(Point& other){
    return sqrt(pow(this->x-other.getx(),2)+pow(this->y-other.gety(),2)+pow(this->z-other.getz(),2));
}

void Point::printpoint(){
    cout<<"Tocka s koordinatama ("<<this->getx()<< " "<< this->gety()<<" "<<this->getz()<<")"<<endl;
}
