#ifndef TARGET_H
#define TARGET_H
#include "C:/Users/Mirko/Desktop/OOP/Vjezba4/Zadatak2.4/include/Weapon.h"


class Target
{
    public:
        Target();
        Target (double x, double y, double visina, double sirina);
        virtual ~Target();
        double getx();
        double gety();
        void setx(double x);
        void sety(double y);
        double getvisina();
        double getsirina();
        void setvisina(double x);
        void setsirina(double y);
        bool hit(Weapon& weapon);



    private:
        double x;
        double y;
        double visina;
        double sirina;
};

#endif // TARGET_H
