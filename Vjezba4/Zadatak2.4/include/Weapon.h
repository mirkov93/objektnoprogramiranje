#ifndef WEAPON_H
#define WEAPON_H


class Weapon
{
    public:
        Weapon();
        Weapon(double x,double y,double z,int capacity, int current);
        double getx();
        double gety();
        double getz();
        void setx(double x);
        void sety(double y);
        void setz(double z);
        int getcapacity();
        int getcurrent();
        void setcapacity(int capacity);
        void setcurrent(int current);
        void shoot();
        void reload();
        virtual ~Weapon();

    private:
        double x;
        double y;
        double z;
        int capacity;
        int current;
};

#endif // WEAPON_H
