#include "Weapon.h"

Weapon::Weapon()
{
    this ->x = 0.0;
    this ->y = 0.0;
    this ->z = 0.0;
    this ->capacity = 30;
    this -> current = this ->capacity;
}
Weapon::Weapon(double x,double y,double z,int capacity, int current)
{
    this ->x = x;
    this ->y = y;
    this ->z = z;
    this ->capacity = capacity;
    this ->current = current;
}
double Weapon::getx()
{
    return this->x;
}
double Weapon::gety()
{
    return this->y;
}
double Weapon::getz()
{
    return this->z;
}
void Weapon::setx(double x)
{
    this ->x = x;
}
void Weapon::sety(double y)
{
    this ->y = y;
}
void Weapon::setz(double z)
{
    this ->z = z;
}
int Weapon::getcapacity()
{
    return this -> capacity;
}
int Weapon::getcurrent()
{
    return this -> current;
}
void Weapon::setcapacity(int capacity)
{
    this -> capacity = capacity;
}
void Weapon::setcurrent(int current)
{
    this -> current = current;
}
void Weapon::shoot()
{
    this->setcurrent(getcurrent()-1);
}
void Weapon::reload()
{
    this->setcurrent(getcapacity());
}
Weapon::~Weapon()
{
    //dtor
}
