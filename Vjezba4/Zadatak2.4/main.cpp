#include <iostream>
#include "Weapon.h"

using namespace std;
int main()
{
    //cout << "Hello world!" << endl;
    Weapon w1;
    Weapon *w2 = new Weapon(3.2, 3, 4.4, 30, 30);
    //cout << w1.getcapacity() << " " << w1.getx();
    w1.shoot();
    cout << w1.getcurrent()<<endl;
    w1.shoot();
    cout << w1.getcurrent()<<endl;
    w1.shoot();
    cout << w1.getcurrent()<<endl;
    w1.shoot();
    cout << w1.getcurrent()<<endl;
    w1.shoot();
    cout << w1.getcurrent()<<endl;
    w1.reload();
    cout << w1.getcurrent()<<endl;
    return 0;
}
