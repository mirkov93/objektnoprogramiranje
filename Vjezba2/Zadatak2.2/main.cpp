#include <iostream>
#include <algorithm>

using namespace std;
void even_odd (int* niz, int l){
    sort (niz,niz+l);
    int odd=0;
    int even=0;
    int temp_e=0;
    int temp_o=0;
    for (int i=0; i<l; i++){
        if(niz[i]%2==0){
            even++;
        }
        else{
            odd++;
        }
    }
    int *even_arr;
    even_arr = new int[even];
    int *odd_arr;
    odd_arr = new int[odd];
    for (int i=0; i<l; i++){
        if(niz[i]%2==0){
            even_arr[temp_e]=niz[i];
            temp_e++;}
        else{
            odd_arr[temp_o]=niz[i];
            temp_o++;
        }
        }
    temp_o=0;
    for (int i=0; i<even; i++){
        niz[i]=even_arr[i];
    }
    for (int i=even; i<l; i++){
        niz[i]=odd_arr[temp_o];
        temp_o++;
    }
}


int main()
{
    int *arr;
    int n;
    cout<< "Unesite velicinu niza"<< endl;
    cin >> n;
    arr=new int[n]; //dinamicko alociranje
    for(int i=0; i<n; i++){
        cout<< "Unesite broj"<<endl;
        cin >> arr[i];
    }
    even_odd(arr,n);
    for (int i=0; i<n; i++){
        cout << arr[i]<< " ";
    }

    delete [] arr; //oslobodit memoriju
    arr=0;
    return 0;
}
