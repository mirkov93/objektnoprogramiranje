#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct Vector {
    int *elementi;
    int logical; //broj elemenata
    int physical; //alocirano memorije
};

Vector vector_new (int init){
    Vector newvec;
    newvec.elementi = new int [init];
    newvec.logical=0;
    newvec.physical=init;
    return newvec;
}


void vector_delete (Vector& vec){
    delete [] vec.elementi;
    vec.logical=0;
    vec.physical=0;
}
Vector check_capacity (Vector& vec){
    if (vec.logical==vec.physical){
        Vector v_temp=vector_new(vec.physical*2);
        for(int i=0; i<vec.logical;i++){
            v_temp.elementi[i]=vec.elementi[i];
            v_temp.logical=vec.logical;
            v_temp.physical=vec.physical*2;
        }
        return v_temp;
    }
    return vec;
}

void vector_print (Vector& vec){
    for(int i=0; i<vec.logical;i++){
        cout<<vec.elementi[i]<<" ";
    }
    cout << endl;
}
void vector_fill (Vector& vec){
    for (int i=vec.logical; i<vec.physical;i++){
            int temp=0;
            cout<<"1 za unijeti novi element, ostalo za izlaz"<<endl;
            cin>>temp;
            if (temp==1){
            cout<<"Unesite element"<<endl;
            cin>>vec.elementi[i];
            vec.logical++;
            vec=check_capacity(vec);
            }
            else {break;}
    }
}

int vector_size (Vector& vec){
    return vec.logical;
}

int vector_front (Vector& vec){
    return vec.elementi[0];
}

int vector_back (Vector& vec){
    return vec.elementi[vec.logical-1];
}

void vector_push_back (Vector& vec, int elem){
    vec.elementi[vec.logical]=elem;
    vec.logical++;
}
void vector_pop_back(Vector& vec){
    vec.logical--;
}
int main()
{
    Vector v1=vector_new(2);
    //cout<<v1.physical<<endl;
    vector_fill(v1);
    vector_print(v1);
    vector_pop_back(v1);
    vector_print(v1);
    vector_push_back(v1,420);
    vector_print(v1);
    vector_fill(v1);
    vector_print(v1);
    //cout<<vector_size(v1)<<endl;
    //cout<<vector_front(v1)<<endl;
    //cout<<vector_back(v1)<<endl;
    return 0;
}
