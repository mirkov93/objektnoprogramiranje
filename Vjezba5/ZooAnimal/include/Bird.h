#ifndef BIRD_H
#define BIRD_H
#include <iostream>
#include <string>
#include "ZooAnimal.h"

class Bird : public ZooAnimal
{
    protected:
        int gestperiod;
        double avgtemp;
        string razmnozavanje;

    public:
        Bird(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated, int gestperiod,double avgtemp,string razmnozavanje);
        friend std::istream& operator>>(std::istream& is, Bird& b);
        friend std::ostream& operator<<(std::ostream& os, Bird& b);
};



#endif // BIRD_H
