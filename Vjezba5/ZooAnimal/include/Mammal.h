#ifndef MAMMAL_H
#define MAMMAL_H
#include <iostream>
#include <string>
#include "ZooAnimal.h"

class Mammal : public ZooAnimal
{
    protected:
        int gestperiod;
        double avgtemp;
        string razmnozavanje;

    public:
        Mammal(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated, int gestperiod,double avgtemp,string razmnozavanje);
        friend std::istream& operator>>(std::istream& is, Mammal& m);
        friend std::ostream& operator<<(std::ostream& os, Mammal& m);
};


#endif // MAMMAL_H
