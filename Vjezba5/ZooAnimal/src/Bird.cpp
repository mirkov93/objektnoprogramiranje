#include "Bird.h"

using namespace std;

Bird::Bird(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje) : ZooAnimal(vrsta, ime, god_rod, kavez, br_obroka, estimated) {
	this->gestperiod=gestperiod;
	this->avgtemp=avgtemp;
	this->razmnozavanje="POLAGANJE JAJA";
}

istream& operator>>(istream& is, Bird& b) {
	cout << "Unesite gestacijski period: ";
	is >> b.gestperiod;
	cout << "Unesite prosje�nu temperaturu: ";
	is >> b.avgtemp;
	return is;
}

ostream& operator<<(ostream& os, Bird& b) {
	os << "Vrsta: " << b.vrsta << "\nIme: " << b.ime << "\nGodina rodenja: " << b.god_rod <<
		"\nBroj Kaveza: " << b.kavez<< "\nBroj dnevnih obroka: " << b.br_obroka <<
		"\nOcekivani zivotni vijek: " << b.estimated << endl;
	os << "Gestacijski period: " << b.gestperiod<< "\nProsjecna temperatura: " << b.avgtemp << "\nNacin razmnozavanja: " << b.razmnozavanje << endl;
	return os;
}


