#include "ZooAnimal.h"
#include <ctime>
#include <iostream>
#include <string>
#include <stdio.h>



ZooAnimal::~ZooAnimal()
{
    //dtor
}

ZooAnimal::ZooAnimal(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated){
            this -> vrsta = vrsta;
            this -> ime = ime;
            this -> god_rod = god_rod;
            this -> kavez = kavez;
            this -> br_obroka = br_obroka;
            this -> estimated = estimated;
            this -> podaci_masa = new Mass [2*estimated];
            for (int i=0;i<(estimated*2);i++)
            {
                podaci_masa[i].god=0;
                podaci_masa[i].masa=0;
            }
        };
ZooAnimal::ZooAnimal(const ZooAnimal& a){
    this -> vrsta = a.vrsta;
    this -> ime = a.ime;
    this -> god_rod = a.god_rod;
    this -> kavez = a.kavez;
    this -> br_obroka = a.br_obroka;
    this -> estimated = a.estimated;
    this -> podaci_masa = a.podaci_masa;
}
void ZooAnimal::promjena_obroka(int stanje){
    if (stanje==2){
        br_obroka++;
    }
    else if (stanje==1){
        br_obroka--;
    }
    else {
        return;
    }
}
void ZooAnimal::ispis(){
    cout << "Print zivotinje"<<endl;
    cout << "Ime: "<<this->ime<<endl;
    cout << "Vrsta: "<<this->vrsta<<endl;
    cout << "Godina rodjenja: "<<this->god_rod<<endl;
    cout << "Broj kaveza: "<<this->kavez<<endl;
    cout << "Broj dnevnih obroka: "<<this->br_obroka<<endl;
    cout << "Ocekivani zivotni vijek zivotinje: "<<this->estimated<<endl;
    cout << "Niz podataka o masi kroz godine: "<<endl;
    for (int i=0; i<(estimated*2); i++){

            if (this->podaci_masa[i].god != 0){
            cout << this->podaci_masa[i].god << " godine " << this->podaci_masa[i].masa << " kilograma"<<endl;
        }
    }
}

int ZooAnimal::promjena_mase(){
    time_t t = time(0);
    struct tm* now = localtime(&t);
    int godina = now->tm_year + 1900;
    double prethodna_masa=0;
    double tekuca_masa=0;
    for (int i=0; i<(estimated*2);i++)
    {
        if (podaci_masa[i].god == godina)
        {
            tekuca_masa=podaci_masa[i].masa;
        }
        else if (podaci_masa[i].god == godina-1)
        {
            prethodna_masa=podaci_masa[i].masa;
        }
    }
    if (tekuca_masa> prethodna_masa+(prethodna_masa/10))
    {
        return 1;
    }
    else if (tekuca_masa<prethodna_masa-(prethodna_masa/10))
    {
        return 2;
    }
    else
    {
        return 0;
    }
}

void ZooAnimal::unos_mase(int god,double masa)
{
    time_t t = time(0);
    struct tm* now=localtime(&t);
    int tekuca = now->tm_year + 1900;
    int index=0;
    for (int i=0; i<(estimated*2); i++)
    {
        if(podaci_masa[i].god==god && god!=tekuca)
        {
            cout<<"Ova godina je unesena. Nije dozvoljeno mijenjati podatke" << endl;
            return;
        }
        else if(podaci_masa[i].god==god && god==tekuca)
        {
            podaci_masa[i].masa=masa;
            return;
        }
        else if(podaci_masa[i].god==0)
        {
            index=i;
            break;
        }

    }
    this->podaci_masa[index].god=god;
    this->podaci_masa[index].masa=masa;
    cout<<"indeks "<<index<<endl;
    cout << "Unijeli ste godinu: "<<podaci_masa[index].god<<" i masu od "<<podaci_masa[index].masa<<" kilograma"<<endl;


}




