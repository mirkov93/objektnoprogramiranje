/*3. U�citati string koji predstavlja re�cenicu. Napisati funkciju koja iz stringa izbacuje sve
praznine koje se nalaze ispred znakova interpunkcije i dodaje praznine nakon znaka
interpunkcije ako nedostaju.
Primjer: Za re�cenicu �Ja bih ,ako ikako mogu , ovu re�cenicu napisala ispravno .�,
ispravna re�cenica glasi: �Ja bih, ako ikako mogu, ovu re�cenicu napisala ispravno.�*/
#include <iostream>
#include <string>
using namespace std;

void lektor (string& s){
    string interpunkcija = ".,;:!?";
    for(int i=0; i<s.size();i++){
        if (s[i]==' '){
            if (interpunkcija.find(s[i+1]) != std::string::npos){
                s.erase(i,1);
            }
        }
        if (interpunkcija.find(s[i]) != std::string::npos){
            if (s[i+1]!=' '){
            s.insert(i+1,1,' ');}
        }

    }
}

int main()
{

    string s;
    cout << "unesite string" << endl;
    getline(cin, s);
    //cout<< s<<endl;
    lektor(s);
    cout<< s<<endl;
    return 0;
}
