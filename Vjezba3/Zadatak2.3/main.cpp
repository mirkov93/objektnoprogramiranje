#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <ctime>
using namespace std;

vector<int> unos (bool flag = false,const int a = 0, const int b = 100, const int n = 5){
    vector<int> v;
    unsigned int ran=0,x=0;

    if (flag){
        for (int i=0; i<n; i++)
        {
            cout<<"Unesite element"<<endl;
            cin>>x;
            //v.insert(i,x);
            v.push_back(x);
        }
    }
    else{
    for (int i=0; i<n; i++){
        ran=rand()%100+1;
        //v.insert(i, rand);
        v.push_back(ran);
    }
    }
    return v;
}
void print_vector (vector<int>& v){
    for (int i=0;i<v.size();i++){
        cout<<v[i]<< " ";
    }
    cout<<endl;
}

vector<int> binary_vector (vector <int>& v1, vector <int>& v2){
    vector<int> v;
    sort(v1.begin(),v1.end());
    sort(v2.begin(),v2.end());
    for (int i=0; i<v1.size();i++){
        if (binary_search (v2.begin(),v2.end(),v1.at(i))==false){
            v.push_back(v1.at(i));
        }
        }
        return v;
}

int main()
{
    srand(time(NULL));
    vector <int> prvi=unos(true);
    vector <int> drugi=unos(true);
    print_vector(prvi);
    print_vector(drugi);
    vector <int> treci=binary_vector(prvi,drugi);
    print_vector(treci);
    return 0;
}
