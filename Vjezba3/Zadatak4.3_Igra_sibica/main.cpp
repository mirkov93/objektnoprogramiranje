#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <string>
#include <stdio.h>

using namespace std;
void intro (){
    cout<<"***DOBRODOSLI, OVO JE IGRA SIBICA!***"<<endl;
    cout<<"Pravila su jednostavna. Na stolu je 21 sibica."<<endl;
    cout<<"Naizmjenicno uzimate sa stola 1,2 ili 3 sibice."<<endl;
    cout<<"Onaj koji uzme POSLJEDNJU sa stola GUBI partiju."<<endl;
    cout<<"Igracu cemo dati malu prednost stoga ce prvo igrati racunalo."<<endl;
    cout<<"SRETNO!"<<endl;

}
bool provjera_unosa (int x){
    if (x>0 && x<4){
        return true;
    }
    else {
        return false;
    }

}
void comp_red (vector<int>& v){
    if (v.size()%4==1)
    {
        int num= rand() % 3+1;
        for (int i=0;i<num;i++){
            v.pop_back();
        }
        cout<<"Racunalo uzima "<<num<<" sibica"<<endl;
        cout<<"NA HRPI JE OSTALO "<<v.size()<<" SIBICA."<<endl;
        cout<<"Vas je red!"<<endl;}

    else if (v.size()%4==0){
        for (int i=0;i<3;i++){
            v.pop_back();
        }
         cout<<"Racunalo uzima "<<3<<" sibica"<<endl;
        cout<<"NA HRPI JE OSTALO "<<v.size()<<" SIBICA."<<endl;
        cout<<"Vas je red!"<<endl;
        }
    else {
        int num= v.size()%4 - 1;
        for (int i=0;i<num;i++){
            v.pop_back();
        }
        cout<<"Racunalo uzima "<<num<<" sibica"<<endl;
        cout<<"NA HRPI JE OSTALO "<<v.size()<<" SIBICA."<<endl;
        cout<<"Vas je red!"<<endl;
    }
}
void igrac_red (vector<int>& v){
    int temp=1;
    int unos;
    cout << "Koliko sibica zelite uzeti?"<<endl;
    cin>>unos;
    while (temp>0){

        if (provjera_unosa(unos)==false)
        {
            cout<< "Nemojte varati, uzmite 1,2 ili 3 sibice"<<endl;
            cout << "Koliko sibica zelite uzeti?"<<endl;
            cin>>unos;
        }
        else {
             if (unos>v.size()){
                cout<< "Nazalost ne mozete uzeti vise nego sto ima sibica na hrpi!"<<endl;
                cout << "Koliko sibica zelite uzeti?"<<endl;
                cin>>unos;
             }
             else{
                    for (int i=0;i<unos;i++){
                    v.pop_back();
                    temp--;}
                    }
        }
    }
    cout<<"Uzeli ste "<<unos<<" sibica."<<endl;
    cout<<"NA HRPI JE OSTALO "<<v.size()<<" SIBICA."<<endl;

}
int main()
{
    srand(time(NULL));
    intro();
    vector<int> sibice (21);
    int red=1;
    while (sibice.size()>=0){
        if (red%2!=0){
            if (sibice.size()>1){
                comp_red(sibice);
                red++;
            }
            else if (sibice.size()==0){
                cout<<"IZGUBILI STE!! =("<<endl;
                cout<<"Vise srece drugi put! =)"<<endl;
                break;
            }
            else{
                cout<<"SVAKA CAST POBJEDILI STE U IGRI SIBICA =)"<<endl;
                break;
            }
        }
        else {
            igrac_red(sibice);
            red++;
        }
    }
    //int kraj;
    //cin>>kraj;
    return 0;
}
