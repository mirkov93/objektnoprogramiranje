#include "Counter.h"
#include <vector>
using namespace std;
Counter::Counter()
{
    total_putnika=0;
}

Counter::~Counter()
{
    //dtor
}

void Counter::add(Vehicle* v)
{
    this->total_putnika += v->passengers();
    cout << v->type() << ", putnika: "<< v->passengers()<<endl;
}

int Counter::total()
{
    return total_putnika;
}
