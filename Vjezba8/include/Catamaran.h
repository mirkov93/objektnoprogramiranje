#ifndef CATAMARAN_H
#define CATAMARAN_H
#include "watercraft.h"


class Catamaran : public watercraft
{
    public:
        Catamaran(int n);
        virtual ~Catamaran();
        unsigned passengers(){
            return broj_putnika;
        }

    protected:
        std::string tip_vozila;
        unsigned broj_putnika;

    private:
};

#endif // CATAMARAN_H
