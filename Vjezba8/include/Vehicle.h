#ifndef VEHICLE_H
#define VEHICLE_H
#include <string>


class Vehicle
{
    public:
        virtual std::string type ()=0;
        virtual unsigned passengers ()=0;
        virtual ~Vehicle();
        Vehicle ();
};

#endif // VEHICLE_H
