#ifndef LAND_VEHICLE_H
#define LAND_VEHICLE_H
#include "Vehicle.h"
#include <string>
#include <iostream>


class land_vehicle : public Vehicle
{
    public:
        land_vehicle();
        virtual ~land_vehicle();
        std::string type () {
            return "Land" ;
        }

    protected:
        std::string tip_vozila;
        unsigned broj_putnika;


    private:
};

#endif // LAND_VEHICLE_H
