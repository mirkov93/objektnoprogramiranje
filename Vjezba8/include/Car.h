#ifndef CAR_H
#define CAR_H
#include "land_vehicle.h"


class Car : public land_vehicle
{
    public:
        Car();
        virtual ~Car();
        unsigned passengers (){
            return broj_putnika;
        }

    protected:
        std::string tip_vozila;
        unsigned broj_putnika;

    private:
};

#endif // CAR_H
