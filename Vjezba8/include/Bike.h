#ifndef BIKE_H
#define BIKE_H
#include "land_vehicle.h"


class Bike : public land_vehicle
{
    public:
        Bike();
        virtual ~Bike();
        unsigned passengers (){
            return broj_putnika;
        }

    protected:
        std::string tip_vozila;
        unsigned broj_putnika;

    private:
};

#endif // BIKE_H
