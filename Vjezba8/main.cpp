#include <iostream>
#include "Vehicle.h"
#include "Counter.h"
#include "Bike.h"
#include "Car.h"
#include "Ferry.h"
#include "Catamaran.h"
#include "land_vehicle.h"
#include "watercraft.h"
#include "Seaplane.h"
#include "aircraft.h"

using namespace std;

int main()
{
    Counter c;
    Vehicle* v[] = {new Bike, new Car, new Catamaran(30), new Ferry(10, 5, 3)};
    size_t sz = sizeof v/sizeof v[0];
    for (unsigned i = 0; i < sz; ++i)
        {c.add(v[i]);}
    std::cout << "ukupno " << c.total() << " putnika" << std::endl;
    for (unsigned i = 0; i < sz; ++i)
    {delete v[i];}
}
